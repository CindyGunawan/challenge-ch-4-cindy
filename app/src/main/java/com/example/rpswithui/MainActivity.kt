package com.example.rpswithui

import android.media.Image
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.rpswithui.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity(){

    private var allowPlaying=true

    private lateinit var selectionP1:String
    private lateinit var selectionC1:String


//    var gameOptions = arrayOf(R.id.comBatu,R.id.comKertas, R.id.comGunting)
//    var gameNumber:Int=0
////    var playerOptions = arrayOf(R.id.playerBatu,R.id.playerGunting,R.id.playerKertas)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main);
        val binding :ActivityMainBinding = ActivityMainBinding.inflate(layoutInflater)

        setContentView(binding.root)

        binding?.batu?.setOnClickListener{
            onPlay("batu")
        }
        binding?.kertas?.setOnClickListener{
            onPlay("kertas")
        }
        binding?.kertas?.setOnClickListener{
            onPlay("gunting")
        }

        binding?.batucom?.setOnClickListener{
            onCom("batu")
        }
        binding?.kertascom?.setOnClickListener{
            onCom("kertas")
        }
        binding?.guntingcom?.setOnClickListener{
            onCom("gunting")
        }


    }
//
//    private fun onFinish(selection: String){
//        allowPlaying=true
//        playerReady=false
//        comReady=false
//        setSelectedIcon()
//    }


    private fun onPlayer(selection:String) {
        if (allowPlaying) {
            selectionP1 = selection
            setSelectedIcon()

        }
    }

    private fun onCom(selection: String){
        if(allowPlaying){
            selectionC1= listOf("batu","gunting","kertas").random()
            setSelectedIcon()
        }
    }



    private fun onPlay(selection:String){
        if(allowPlaying){
            selectionC1= listOf("batu","gunting","kertas").random()
            selectionP1=selection
            setSelectedIcon()
            allowPlaying=false
        }
    }



    private fun setSelectedIcon(){
        when(selectionP1){
            "batu" -> (R.id.batu)
            "kertas" -> (R.id.kertas)
            "gunting" -> (R.id.gunting)
        }

        when (selectionC1){
            "batu" -> (R.id.batu)
            "kertas" -> (R.id.kertas)
            "gunting" -> (R.id.gunting)

        }
    }

    private fun getResult():String{
        return if (selectionP1==selectionC1)
            "tie"
        else if (selectionP1=="batu" && selectionC1=="gunting"||
                selectionP1=="kertas" && selectionC1=="batu" ||
                    selectionP1=="gunting" && selectionC1=="kertas")
            "P1"
        else
            "P2"
    }

    private fun setWinner(){
        if(getResult()=="tie"){
            findViewById<TextView>(R.id.resultDraw)
        }
        else if(getResult()=="P1"){
            findViewById<TextView>(R.id.playerMenang)
        }
        else
            findViewById<TextView>(R.id.comMenang)
    }

    override fun onDestroy(){
        super.onDestroy()
        findViewById<ImageView>(R.id.buttonReset)

    }






//        val playerBatu: ImageView = findViewById(R.id.playerBatu)
//        val playerKertas: ImageView = findViewById(R.id.playerKertas)
//        val playerGunting: ImageView = findViewById(R.id.playerGunting)
//        val reset: ImageView = findViewById(R.id.buttonReset)
////        val comMenang: TextView = findViewById(R.id.comMenang)
////        val playerMenang: TextView = findViewById(R.id.playerMenang)
////        val gameTie: TextView = findViewById(R.id.resultDraw)
//
//        playerBatu.setOnClickListener (this)
//        playerKertas.setOnClickListener (this)
//        playerGunting.setOnClickListener (this)
//
//        reset.setOnClickListener(this)
//
//    }
//
//    override fun onClick(v:View){
//        val id = v.id
//
//        when(id){   //checking for button id selected
//            R.id.playerBatu -> {
//                gameNumber = 1 //value for rock
//                computerPlay() //function provides random cpu image
//            }
//            R.id.playerKertas -> {
//                gameNumber = 2
//                computerPlay()
//            }
//            R.id.playerGunting -> {
//                gameNumber = 3
//                computerPlay()
//            }
//        }
//    }
//
//    private fun computerPlay() {
//        val imageId = (0..(gameOptions.size - 1)).random()
//        checkWinner(imageId)
//    }
//
//    private fun checkWinner(imageId : Int) {
//        // game logic - gets user image by value and checks against cpu image
//        if(gameNumber == 1 && imageId == 0) {
//            showWinner(2)
//        } else if(gameNumber == 1 && imageId == 1){
//            showWinner(1)
//        } else if(gameNumber == 1 && imageId == 2){
//            showWinner(0)
//        } else if(gameNumber == 2 && imageId == 0){
//            showWinner(0)
//        } else if(gameNumber == 2 && imageId == 1){
//            showWinner(2)
//        } else if(gameNumber == 2 && imageId == 2){
//            showWinner(1)
//        } else if(gameNumber == 3 && imageId == 0){
//            showWinner(1)
//        } else if(gameNumber == 3 && imageId == 1){
//            showWinner(0)
//        } else if(gameNumber == 3 && imageId == 2){
//            showWinner(2)
//        }
//    }
//
//    fun showWinner(result:Int) {
//
//        //winning logic
//        when (result) {
//            0 ->  findViewById <TextView>(R.id.comMenang).apply {text}
//            1 ->  findViewById <TextView> (R.id.playerMenang).apply { text }
//            else -> findViewById <TextView> (R.id.resultDraw).apply {text}
//        }
//    }
//
//    fun reset (v: View) {
//        findViewById<TextView>(R.id.reset).apply {text}
//    }


//
//        var gameChoice = getGameChoice(options)
//
//        var userOption = getUserOption(option)
//
//        printResult(gameChoice, userOption)
//    }
//    fun getGameChoice(optionParam: Array<Int>) =
//        optionParam[(Math.random() * optionParam.size).toInt()]
//
//    fun getUserOption(optionParam: Array<Int>): String {
//        var isValid = false
//        var userChoice = ""
//
//        while (!isValid) {
//            println("Masukan Jawaban")
//
//            for (items in optionParam) println(items)
//            println(".")
//
//            var userInput = readLine()
//
//            if (userInput != null && userInput in optionParam) {
//                isValid = true
//
//            }
//
//        }
//        return userChoice
//
//    }
//
//    fun printResult(gameChoice: Int, userChoice: String) {
//
//        var result: String
//
//        if (gameChoice == userChoice) result = "tie"
//        else if ((gameChoice == R.id.playerKertas && userChoice == R.id.comKertas) ||
//            (gameChoice == R.id.playerBatu && userChoice == R.id.comGunting) ||
//            (gameChoice == R.id.playerGunting && userChoice == R.id.playerBatu)
//        )
//
//            result = "You Lost!"
//        else
//            result = "You Win!"
//
//        println("The game chose $gameChoice and You chose $userChoice. The result is $result")
    }


